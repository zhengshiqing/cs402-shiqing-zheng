#define MAXNAME 64
#define MAXEMP 1024
// construct the person(name,surname, id, salary)
struct person{
    char name[MAXNAME], surname[MAXNAME];
    int id, salary;
};

// open the file function
int open_file(char *fname){
    if(fopen(fname,"r")==NULL)
       return -1;
    return 0;
}
  // read the int 
int read_int(int *address){
    if(scanf("%d", address)==0)
       return -1;
    return 0;
}
// read the float
int read_float(float *address){
    if(scanf("%f", address)==0)
       return -1;
    return 0;
}
//read string
int read_string(char *address){
    if(scanf("%s", address)==0)
       return -1;
    return 0;
}
// the function of close file
void close_file(FILE *fname){
    fclose(fname);
}
// the function of compare by ID;
int comparedById(const void *a, const void *b) {
    struct person *employee1 = (struct person *)a;
    struct person *employee2 = (struct person *)b;
    return (*employee1).id - (*employee2).id;
}
int compare( const void* a, const void* b) {
    int int_a = * ( (int*) a );
    int int_b = * ( (int*) b );
    return int_a - int_b;
}
// the function of binary search
int binary_search(const int arr[],int l, int r, int x)
{
    if(r>=l)
    {
        int mid =l+(r-l)/2;
        /* if the element is present at the middle itself*/ 
        if(arr[mid]==x)
        return mid;
        /* if element is smaller than mid ,then it can only be present in left subarray*/
        if(arr[mid]>x)
        return binary_search(arr, l, mid - 1, x);
        /* else the element can only be present in right subarray*/
        return binary_search(arr, mid + 1, r, x);

    }
    /* we reach here when element is not present in array*/
    return -1;
}
// function of printf
void print_by_key(struct person employee[MAXEMP], int key){
     //Given structure and id,this function prints the employee record if it exists
    printf("------------------------------------\n");
    printf("NAME              SALARY      ID\n");
    printf("%-10s %-10s %10d %10d \n", employee[key].name, employee[key].surname, employee[key].salary, employee[key].id);
    printf("------------------------------------\n");
}
// function of search surname 
int search_surname(struct person employee[MAXEMP], int n_emp, char surname[]){
    for (int i=0; i < n_emp; i++){
        if(strcmp(employee[i].surname,surname)==0)
        {
            return i;
        }
    }
    return -1;
}
//int print_by_name(struct person employee[SIZE], char emp_name[256]){
//   /*given structure and last name, this function prints the employee record
//   if is exists*/
//  int length = sizeof(employee)/sizeof(employee[0]);
// for(int i =0; i< length; i++){
//  if(employee[i].first_name--emp_name){
//    printf("NAME\t\t\t\t\t\tSALARY\t\tID\n");
//    print("------------------------------------\n");
//    print("%s\t\t%s\t\t%d\t\t%d\n", employee[i].first_name, employee[i].last_name, employee[i].salary, employee[i].six_digit_id);             SALARY      ID\n);
//    print("------------------------------------\n");
//      return 0
//    } 
//  }
//    return 0;
//}
