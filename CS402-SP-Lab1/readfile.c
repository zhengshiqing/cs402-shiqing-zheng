#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"readfile.h"
int main(int argc, char *argv[])
{
    // read the file of input.txt
    if(argc<2){
     printf("pass filename to read from ...\n"); 
     return 0;
    }
    // constructor the variable of person employee(id, key, salary,confirm)
    char *fname=argv[1], name[MAXNAME],surname[MAXNAME];
    struct person employee[MAXEMP];
    int option, emp_id, key, salary, confirm;
    int n_emp = 0, list[MAXEMP];
    //open the file of the data of input.txt
    if(open_file(fname) == -1){
        printf("error reading file\n");
        return -1;
    }

    FILE *fp = fopen(fname,"r");
    // while function; read the the data of employee one by one;
    while(!feof(fp))
    {
        fscanf(fp,"%d %s %s %d\n", &employee[n_emp].id, &employee[n_emp].name, &employee[n_emp].surname, &employee[n_emp].salary);
        list[n_emp] = employee[n_emp].id;
        n_emp++;

    }
    // close the file of input.txt
    fclose(fp);
    // qsort list the data of employee by ID,use the function of compare from the readfile.h
    qsort(employee, 14, sizeof(struct person), comparedById);
    qsort(list, 14, sizeof(int), compare);
    // while function
    while(1){
        printf("*******************\n");
        printf("employee DB Menu:\n");
        printf("*******************\n");
        printf("[1] Print the Database\n");
        printf("[2] lookup by ID\n");
        printf("[3] lookup by Last Name\n");
        printf("[4] Add an Employee\n");
        printf("[5] Quit\n");
        printf("*******************\n");
        printf("Enter your option:\n");
        // read the option
        read_int(&option);
        // scanf("%d", &option);
        // if option==1, print the database and qsort the list by ID;
        if(option==1){
             printf("------------------------------------\n");
             printf("NAME              SALARY      ID\n");
             printf("------------------------------------\n");
             for(int i=0; i<n_emp;i++){
                 printf("%-10s %-10s %10d %10d \n", employee[i].name, employee[i].surname, employee[i].salary, employee[i].id);
             }
            printf("------------------------------------\n");
        }
        // if option==2, scanf the ID, and use the binary search to find the data of the ID;
        else if(option==2){
            printf("enter a 6 digit employee id:\n ");
            read_int(&emp_id);
            // scanf("%d", &emp_id);
            key=binary_search(list, 0, n_emp, emp_id);
            if(key == -1){
                printf("employee with id %d not found. \n", emp_id);
            }
            else 
               print_by_key(employee,key);

        }
        // if option ==3, scanf the surname,and use the search- surname
        else if(option==3){
         printf("enter employee's surname:\n");
         read_string(&surname);
         // scanf("%s", surname);
         key= search_surname(employee,n_emp, surname);
         if(key == -1){
             printf("employee with id %d not found. \n", emp_id);
         }
         else
          print_by_key(employee,key);
         

        }
        //if option==4, add the data to the database
        else if(option==4){
            printf("enter the first name of the employee:\n");
            read_string(&name);
            // scanf("%s,name");
            printf("enter the surname of the employee:\n");
            read_string(&surname);
            // scanf("%s,surname");
            printf("enter the salary of the employee(between $30,000 and $150,000):\n");
            read_int(&salary);
            // scanf("%d,&salary");
            printf("do you wanna to add the following employee to the database\n");
            printf("enter 1 for yes, 0 for no\n");
            read_int(&confirm);
            // scanf("%d,&confirm");
            // if confirm==1, 
            if(confirm == 1){
                // if the salary is llegal between 30000~150000, the employee is llegal;
               if(salary>=30000&&salary<=150000){
                 int cur_id=employee[n_emp-1].id;
                 int new_id = cur_id+1;
                 strcpy(employee[n_emp].name, name);
                 strcpy(employee[n_emp].surname, surname);
                 employee[n_emp].salary = salary;
                 employee[n_emp].id = new_id;
                 list[n_emp]=employee[n_emp].id;
                 n_emp++;
               }
               else {
                   printf("salary invalid. please enter another salary amount.\n");
               }
            }
            else {
                printf("dont wanna to add the person to the address.\n");
            }
        }
        //if the option==5,quit;
        else if(option==5){
            printf("googlebye\n");
            break;
        }
        
        else{
            printf("%d is not between 1 and 5, please make your choice again.\n");
            continue;

        }


    }
}
