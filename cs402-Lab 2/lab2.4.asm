.data 0x10010000
	var1: .word 33	# var1 is a word (32 bit) with the initial value 33
	var2: .word 08	# var2 is a word (32 bit) with the initial value 08
.extern        ext1 4		# ext1 is a word (32 bit) in the extern space
.extern	    ext2 4		# ext2 is a word (32 bit) in the extern space
	
.text
.globl main	
	main: 
	lw $t0,var1	# load content of var1 to register t0
	lw $t1,var2	# load content of var2 to register t1
	
	sw $t0,ext1	# store content of var1 to address of ext1
	sw $t1,ext2	# store content of var2 to address of ext2
	
	# Code for Test

	#li $v0, 1	# system call for print_str

	#lw $a0, 0($t2)
	#syscall
	#lw $a0, 0($t3)
	#syscall

	li $v0,10	# system call for exit
	syscall		# exit
