		.data 0x10010000
var1: 	.word 83   	#var1 is a word (32 bit) with the initial value 83	
var2: 	.word 104
var3:	.word 111
var4:	.word 119

first:	.byte 's'
last:	.byte 'z'


		.text
		.globl main

main:		
                                ori $8,$0,6
                                addu $9, $0, $8
		
                                lui $10, 4097 	# get var1 address
		lui $1, 4097   	# get var4 address
                                ori $13,$1,12 
 
		lui $1, 4097	# temparily save var1 value in register $t3
		lw $11, 12($1)
                                lui  $1, 4097           # temparily save var4 value in register $t4
                                lw $12, 0($1)	
		
		sw $11, 0($10)	# store original var4 into var1 address
		sw $12, 0($13)	# store original var1 into var4 address

		lui $1,  4097 	# get var2 address
                                ori $10, $1, 4   
		lui $1,  4097 	# get var3 address
                                ori $13, $1, 8 
		lui $1,  4097 	# temparily save var3 value in register $t6
                                lw $11, 8($1)
                                lui $1, 4097              # temparily save var2 value in register $t7
		lw $12, 4($1)	
		
		sw $14, 0($10)	# store original var3 into var2 address
		sw $15, 0($13)	# store original var2 into var3 address

# restore now the return address in $ra and return from main
		ori $2, $0, 10
                                syscall


