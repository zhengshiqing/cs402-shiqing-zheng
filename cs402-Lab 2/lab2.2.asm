
		.data 0x10010000
var1: 	.word 83   	#var1 is a word (32 bit) with the initial value 83	
var2: 	.word 104
var3:	.word 111
var4:	.word 119

first:	.byte 's'
last:	.byte 'z'


		.text
		.globl main

main:		addu $s0, $ra, $0	# save $31 in $16

		la $t2, var1	# get var1 address
		la $t5, var4	# get var4 address

		lw $t3, var4	# temparily save var1 value in register $t3
		lw $t4, var1	# temparily save var4 value in register $t4
		
		sw $t3, 0($t2)	# store original var4 into var1 address
		sw $t4, 0($t5)	# store original var1 into var4 address

		la $t2, var2	# get var2 address
		la $t5, var3	# get var3 address

		lw $t6, var3	# temparily save var3 value in register $t6
		lw $t7, var2	# temparily save var2 value in register $t7
		
		sw $t6, 0($t2)	# store original var3 into var2 address
		sw $t7, 0($t5)	# store original var2 into var3 address

# restore now the return address in $ra and return from main
		addu $ra, $0, $s0 	# return address back in $31
		jr $ra 		# return from main


