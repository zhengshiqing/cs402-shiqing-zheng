
###########condition depended
WIN10 64 profession 
vscode version：1.24.1 
launch.json version：0.2.0 
tasks.json version：2.0.0 
c_cpp_properties.json version:4
settings.jason   // setting in the vscode 
mingw-w64 version：8.1.0



###########Deployment steps
1. download & install vscode
  Address: https://code.visualstudio.com/
2. Install C / C + + plug-in in vscode
    c/c++,
    c/c++ Clang command
    include Autocomplete
    Bracket Pair Colorizier
    Code Runner
    One Dark Pro
    vscode-clangd

3. download & install MinGW
 1)load MinGW:http://sourceforge.net/projects/mingw/files/latest/download 
 2)store in c:\mingw
 3)install--c:\mingw, intall the X86_64-posix-seh,MinGW-W64 GCC-8.1.0
 4)Add LLVM to the system PATH for all users，path：C:\mingw64\bin
 5)check the condition whether succeed:
   cmd---g++ -v ---gcc -v --- gdb -v;
4. restart the computer
5.set the IDE
 1) Open the vscode,new file folder
 2)new file folder in the new file folder,rename the folder as the name .vscode;
 3)new 4 files as:launch.json，settings.json，tasks.json,c_cpp_properties.json;which are in the .vscode folder.
6. gulp build
7.test the samples.
8.build the new file "basicstats.c". and the data of samples "small.txt &large.txt".
9. the project of the basicstats.c 
 1)include the .h          // Calling function from scratch file
 2) function           //read the data from the input.txt
   2.1 open the file
   2.2 the function to get mean
   2.3 the function to get SD
   2.4 the function to get median
   2.5 the function to sort array
   2.6 main function 

10. test the project --use the breakpiont
 10.1 use the code runner button to check the code;
 10.2 running the code use"gcc -o basicstats.exe basicstats.c"
 10.3 at the terminal input the different types of data to test the project.
  10.3.1 input ".\basicstats.exe small.txt"
  output: 
  The mean is: 85.776175
  The DS is: 90.380406
  The median is:272.047348
  10.3.2 input".\basicstats.exe large.txt"
  output
  The mean is: 2035.600000
  The DS is: 1496.152858
  The median is:1502.500000


###########V1.0.0 Version content update