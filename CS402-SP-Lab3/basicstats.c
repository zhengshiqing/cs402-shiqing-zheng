#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
//open file, if file==null,exit;
FILE *open_file(char *fname){
    FILE *fp = fopen(fname,"r");
    if(fp== NULL){
        printf("Error readfing file\n");
        exit(0);
    }
    return fp;
}
//the function to get mean
double get_mean(float *p, int n){
    double sum = 0;
    for(int i=0; i<n; i++)
       sum = sum+p[i];
       double mean = sum/n;
       return mean;
}
//the function to get SD
double get_sd(float *p, int n){
    //sqrt((sum((xi-mean)^2))/N);
    double sd = 0;
    double mean = get_mean(p,n);
    for(int i=0; i<n; i++){
        sd+=pow(p[i]-mean, 2);
    }
    return sqrt(sd/n);

}
//the function to get median
double get_median(float *cur_arr, int n){
    // the number of array is even number;
    if (n%2==1)
     return(cur_arr[n/2]);
     // the number of array is Odd number
     else
     {
         double min = cur_arr[n/2-2];
         double max = cur_arr[n/2];
         return((min+max)/2);
         
     }
      
}
//the function to sort array
void swap(float *xp, float *yp){
    float temp = *xp;
    *xp=*yp;
    *yp= temp;
}
float sort(float *cur_arr, int n){
    int i, j, min_idx;
    //one by one move boundary of 
    for ( i = 0; i < n-1; i++){
       //find the minimum element 
       min_idx = i;
       if(cur_arr[i]<cur_arr[min_idx])
          min_idx = j;
       //swap the found minimum element with
       swap(&cur_arr[min_idx], &cur_arr[i]);
    
    }  
}
int main(int argc, char *argv[]){
    //if argc<2,warning;
    if(argc<2){
        printf("pass filename to read from...\n");
        return 0;
    }
    //array index is 20;
    int n = 20, length_arr=0;
    float temp;
    // store the cur_arr
    float *cur_arr = (float*)malloc(n*sizeof(float));
    char *fname = argv[1];
    FILE *fp = open_file(fname);
    while (!feof(fp)){
        fscanf(fp, "%f\n",(cur_arr+length_arr));
        length_arr++;
        if(length_arr == n){
           // the current length of array is equal to max capacity
          // create a new array with twice size 
          float *new_arr=(float*)malloc(n*2*sizeof(float));
          //copy values from the old temp array to new array;
          memcpy(new_arr,cur_arr,length_arr*sizeof(float));
          //free memory
          free(cur_arr);
          cur_arr = new_arr;
          // increase the size of n;
          n=n*2;

          }
          
        }

        fclose(fp);
        double mean = get_mean(cur_arr, length_arr);
        double sd = get_sd(cur_arr, length_arr);
        printf("The mean is: %f\n", mean);
        printf("The DS is: %f\n", sd);
        sort(cur_arr,length_arr);
        double median= get_median(cur_arr,length_arr);
        printf("The median is:%f\n",median);
 
}
