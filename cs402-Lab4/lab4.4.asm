.data
msg: .asciiz "the largest number is: "

.text
.globl main
main:
    li $v0, 5           # read_int
    syscall
    move $t0, $v0
    li $v0, 5
    syscall
    move $t1, $v0

    addi $sp, $sp, -4   # save $ra
    sw $ra, 4($sp)   
    addi $sp, $sp, -8   # allocate stack space for 2 words
    sw $t0, 4($sp)      # push arguments into stack
    sw $t1, 8($sp)
    jal Largest
    addi $sp, $sp, 8    # restore $sp position
    lw $ra, 4($sp)      # restore $ra
    addi $sp, $sp, 4
    jr $ra

Largest:
    li $v0, 4
    la $a0, msg
    syscall             # print message
    lw $a0, 4($sp)      # load parameters from stack
    lw $a1, 8($sp)
    slt $t0, $a0, $a1
    blez $t0, output    # if $t0 <= 0 ($a0 >= $a1), goto output
    move $a0, $a1       # $a0 < $a1, we should output $a1
output:
    li $v0, 1
    syscall             # print largest number
    jr $ra


