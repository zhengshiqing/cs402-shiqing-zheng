#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include "readfile.h" 

int main(int argc, char *argv[]) 
{
    if (argc < 2){
        printf("Pass filename to read from...\n");
        return 0; 
    }
    char *fname = argv[1], name[MAXNAME], surname[MAXNAME];
    struct person employee[MAXNAME];
    int option, emp_id, key, salary, confirm, M; 

    int n_emp = 0;
    if(open_file(fname) == -1){
        printf("Error reading file");
        return -1;
    }
    FILE *fp = fopen(fname, "r");
    while(!feof(fp)){
        fscanf(fp,"%d %s %s %d\n", &employee[n_emp].id, &employee[n_emp].name, &employee[n_emp].surname, &employee[n_emp].salary);
        n_emp++;
    }
    fclose(fp);

    while(1){
        printf("**********************************\n");
        printf("Employee DB Menu:\n");
        printf("**********************************\n");
        printf(" (1) Print the Database\n");
        printf(" (2) Lookup by ID\n");
        printf(" (3) Lookup by Last Name\n");
        printf(" (4) Add an Employee\n");
        printf(" (5) Quit\n");
        printf(" (6) Remove an employee\n");
        printf(" (7) Update an employee's information\n");
        printf(" (8) Print the M employees with the highest salaries\n");
        printf(" (9) Find all employees with matching last name\n");
        printf("**********************************\n");
        printf("Enter your option: ");
          //read_int(&option);
        scanf("%d",&option);
      
        //// if option==1, print the database and qsort the list by ID;
        if(option == 1){
            print_structure(employee, n_emp);
            //break;   
        }
        //if option==2, scanf the ID, and use the binary search to find the data of the ID;
        else if(option == 2){
            printf("Enter a 6 digit employee id: ");
            //read_int(&emp_id);
            scanf(" %d", &emp_id);
            key = binary_search(employee, 0, n_emp, emp_id);
            if(key == -1)
                printf("Employee with id %d not found in DB\n", emp_id);
            else
                print_by_key(employee, key);
            //break;
        }
        // if option ==3, scanf the surname,and use the search- surname
        else if(option == 3){
            printf("Enter Employee's last name (no extra spaces): ");
            //read_string(&surname)
            scanf(" %s", &surname);
            key = search_lname(employee, n_emp, surname);
            if(key == -1)
                printf("Employee with last name %s not found in DB\n", surname);
            else
                print_by_key(employee, key);
        }
        //if option==4, add the data to the database
        else if(option == 4){
            printf("Enter the first name of the employee: ");
            //read_string(&name);
            scanf("%s", name);
            printf("Enter the last name of the employee: ");
            scanf("%s", surname);
            printf("Enter employee's salary (30000 to 150000): ");
            scanf(" %d", &salary);
            printf("do you want to add the following employee to the DB? \n");
	        printf("%s %s, salary: %d \n",name, surname, salary);
            printf("Enter 1 for yes, 0 for no: ");
            //read_int(&confirm);
            scanf("%d", &confirm);
            // if the salary is llegal between 30000~150000, the employee is llegal;
            if (confirm == 1){
                if((salary >= 30000) && (salary <= 150000)){
                    int cur_id = employee[n_emp-1].id;
                    int new_id = cur_id+1;
                    strcpy(employee[n_emp].name, name);
                    strcpy(employee[n_emp].surname, surname);
                    employee[n_emp].salary = salary;
                    employee[n_emp].id = new_id;
                    n_emp++;
                } else {
                    printf("Salary range invalid. Please enter a value between 30000 and 150000\n");
                }
            }
        }
         //if the option==5,quit;
        else if(option == 5){
            printf("Are you sure you wanna to exit? (If anything changed, it will be updated to the input.txt file.)\n ");
            printf("Enter 1 for yes, 0 for no: ");
            //read_int(&confirm);
            scanf("%d", &confirm);
            if (confirm == 1){
                FILE *fp = fopen(fname, "w");
                for(int i=0; i<n_emp; i++){
                    fprintf(fp, "%d %s %s %d\n",employee[i].id, employee[i].name, employee[i].surname, employee[i].salary);
                }
                fclose(fp);
            }
            printf("goodbye!\n");
            break;
        }
         //option==6, delete the employee;
        else if(option == 6){
            //if delete, warning users
            printf("Warning! You are gonna to delete an employee!\n");
            printf("Enter a 6 digit employee id: ");
            //read_int(&emp_id);
            scanf(" %d", &emp_id);
             //search the id and output the employee
            key = binary_search(employee, 0, n_emp, emp_id);
              //if key==-1, not found
            if(key == -1)
                printf("Employee with id %d not found in DB\n", emp_id);
            else{
                print_by_key(employee, key);
                printf("Are you sure to delete the above employee?\nEnter 1 to confirm, enter 0 to cancel the opration.\n");
                //read_int(&confirm);
                scanf("%d", &confirm);
                if (confirm == 1){
                    delete_by_key(employee, n_emp, key);
                    printf("Sucessfully deleted!\n");
                    n_emp--;
                } 
                else {
                    printf("The above employee was not deleted.\n");
                }
            }
        }
             //option==7, update the employee
        else if(option == 7){
            printf("Enter a 6 digit employee id: ");
            //read_int(&emp_id);
            scanf(" %d", &emp_id);
            key = binary_search(employee, 0, n_emp, emp_id);
            if(key == -1)
                printf("Employee with id %d not found in DB\n", emp_id);
            else{
                //print the employee
                print_by_key(employee, key);
                printf("Enter the updated first name of the employee: ");
                //read_int(&name);
                scanf("%s", name);
                printf("Enter the updated last name of the employee: ");
                scanf("%s", surname);
                printf("Enter updated employee's salary: ");
                scanf(" %d", &salary);
                printf("Do you want to update the employee with the following information? \n");
                printf("%s %s, salary: %d \n",name, surname, salary);
                printf("Enter 1 for yes, 0 for no: ");
                scanf("%d", &confirm);
                if (confirm == 1){
                    update_by_key(employee, key, name, surname, salary);
                    printf("Sucessfully update! The updated information is as followed: \n");
                    print_by_key(employee, key);
                } 
                else {
                    printf("The above employee was not updated.\n");
                }
            }  
        } 
          //option==8, list the topM salary's employee's information
        else if(option == 8){
            printf("Top ? salary you wanna to see?\n");
            scanf(" %d", &M);
            // Create a temporary structure to look up and another to store
            struct person temp[MAXNAME], max_n[M];
            int temp_n_emp = n_emp;            
            // Create a structure with value as original structure
            for(int i=0; i<n_emp; i++){
                strcpy(temp[i].name, employee[i].name);
                strcpy(temp[i].surname, employee[i].surname);
                temp[i].salary = employee[i].salary;
                temp[i].id = employee[i].id;
            }
            // top M salary employee list
            for(int i=0; i<M; i++){
                int largest = get_largest_salary_index(temp, temp_n_emp);
                // Update the values in max_n
                strcpy(max_n[i].name, temp[largest].name);
                strcpy(max_n[i].surname, temp[largest].surname);
                max_n[i].salary = temp[largest].salary;
                max_n[i].id = temp[largest].id;
                // delete the largest element and repeat
                delete_by_key(temp, temp_n_emp, largest);
                temp_n_emp--;
            }
            // print top M
            print_structure(max_n, M);
        }
        //option==9; search the lastname of the employee
        else if(option == 9){
            printf("Enter the last name of the employee: ");
            scanf("%s", surname);
            all_last_name(employee, n_emp, surname);
        }
        else{
            printf("Hey, %d is not between 1 and 9, try again...\n", option);
            continue;
        }
    }
}