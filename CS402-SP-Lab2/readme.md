
###########condition depended
WIN10 64 profession 
vscode version：1.24.1 
launch.json version：0.2.0 
tasks.json version：2.0.0 
c_cpp_properties.json version:4
settings.jason   // setting in the vscode 
mingw-w64 version：8.1.0


###########Deployment steps
1. download & install vscode
  Address: https://code.visualstudio.com/
2. Install C / C + + plug-in in vscode
    c/c++,
    c/c++ Clang command
    include Autocomplete
    Bracket Pair Colorizier
    Code Runner
    One Dark Pro
    vscode-clangd

3. download & install MinGW
 1)load MinGW:http://sourceforge.net/projects/mingw/files/latest/download 
 2)store in c:\mingw
 3)install--c:\mingw, intall the X86_64-posix-seh,MinGW-W64 GCC-8.1.0
 4)Add LLVM to the system PATH for all users，path：C:\mingw64\bin
 5)check the condition whether succeed:
   cmd---g++ -v ---gcc -v --- gdb -v;
4. restart the computer
5.set the IDE
 1) Open the vscode,new file folder
 2)new file folder in the new file folder,rename the folder as the name .vscode;
 3)new 4 files as:launch.json，settings.json，tasks.json,c_cpp_properties.json;which are in the .vscode folder.
6. gulp build
7.test the sample of the project like the "hello"
8.build the new file "readfile.c" and the new file "readfile.h".
9. the project of the readfile.c； 
 1)include the .h          // Calling function from scratch file
 2)main function           //read the data from the input.txt
   2.1 read the file
   2.2 read the data and qsort list the date by ID;
 3) option1-9
  3.1[1] Print the Database
  3.2[2] lookup by ID
  3.3[3] lookup by Last Name
  3.4[4] Add an Employee
  3.5[5] Quit
  3.6[6] Remove an employee
  3.7[7] Update an employee's information
  3.8[8] Print the M employees with the highest salaries
  3.9[9] Find all employees with matching last name
10. the function of readfile.h
 10.1 function 1:define the maxname and maxemp;
 10.2 function 2: open file
 10.3 function 3: read int, float, string;
 10.4 function 4: close file
 10.5 function 5: compare function
 10.6 function 6: binary_search
 10.7 function 7: printf 
 10.8 function 8: search_surname
 10.9 function 9: print_structure
 10.10 function 10: binary_search_lis
 10.11 function 11: print_by_key
 10.12 function 12: delete_by_ke
 10.13 function 13: get_largest_salary_index
 10.14 function 14: update_by_key
 10.15 function 15: search_lname
 10.16 function 16: all_last_name
11. test the project --use the breakpiont
 11.1 use the code runner button to check the code;
 11.2 running the code
 11.3 at the terminal input the:" PS C:\Users\zhengshiqing3\Desktop\code\CS402-SP-Lab1> .\readfile.exe input.txt"
 11.4 input the 1, check the result;(output the database)
 11.5 input the 2, check the result; output:"enter a 6 digit employee id:"
 11.6 input the 3-9,check the result;
 11.7 input the namber >9, check the result;
12. print the output;

###########V1.0.0 Version content update