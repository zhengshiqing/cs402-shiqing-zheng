#define MAXNAME  64
#define MAXEMP  1024

struct person {
    char name[MAXNAME], surname[MAXNAME];   
    int  id, salary;
};

int open_file(char *fname){
    if(fopen(fname, "r") == NULL)
        return -1;
    return 0;
}

int read_int(int *address){
    if(scanf("%d", address) == 0)
        return -1;
    return 0;
}

int read_float(float *address){
    if(scanf("%f", address) == 0)
        return -1;
    return 0;
}

int read_string(char *address){
    if(scanf("%s", address) == 0)
        return -1;
    return 0;
}

void close_file(FILE *fname){
    fclose(fname);
}
// construct the function of print_structure
void print_structure(struct person employee[MAXNAME], int n_emp){
    printf("----------------------------------------------\n");
    printf(" NAME                       SALARY      ID\n");
    printf("----------------------------------------------\n");
    for(int i=0; i<n_emp; i++)
        printf(" %-10s %-10s %10d %10d \n", employee[i].name, employee[i].surname, employee[i].salary, employee[i].id) ;
    printf("----------------------------------------------\n");            
}
// construct the function of binary_search
int binary_search(struct person employee[MAXNAME], int l, int r, int x){
    int arr[MAXEMP];
    for(int i=0;i<r;i++){
        arr[i] = employee[i].id;
    }
    int key = binary_search_list(arr, l, r, x);
    return key;
}
// construct the function of binary_search_list
int binary_search_list(const int arr[], int l, int r, int x){
    if (r >= l) { 
        int mid = l + (r - l) / 2; 
        // If the element is present at the middle 
        // itself 
        if (arr[mid] == x) 
            return mid; 
        // If element is smaller than mid, then 
        // it can only be present in left subarray 
        if (arr[mid] > x) 
            return binary_search_list(arr, l, mid - 1, x); 
        // Else the element can only be present 
        // in right subarray 
        return binary_search_list(arr, mid + 1, r, x); 
    } 
    // We reach here when element is not 
    // present in array 
    return -1; 
}
// construct the function of print_by_key
void print_by_key(struct person employee[MAXNAME], int key){
    /* Given structure and id, this function prints the employee record
    if it exists */
    printf("----------------------------------------------\n");
    printf(" NAME                       SALARY      ID\n");
    printf(" %-10s %-10s %10d %10d \n", employee[key].name, employee[key].surname, employee[key].salary, employee[key].id) ;
    printf("----------------------------------------------\n");
}
//construct the function of delete_by_key
void delete_by_key(struct person *ptr, int n_emp, int key){
    /* Given structure and id, this function deletes the employee record*/
    struct person employee[MAXNAME];
    // Create a structure with value as original structure
    for(int i=0; i<n_emp; i++){
        strcpy(employee[i].name, ptr->name);
        strcpy(employee[i].surname, ptr->surname);
        employee[i].salary = ptr->salary;
        employee[i].id = ptr->id;
        ptr++;
    }
    for(int i=0; i<n_emp; i++)
        ptr--;
    // Delete the value (moves the index in array)
    for (int c = 0; c < n_emp-1; c++){
        if(c >= key){
            int nextindex = c+1;
            ptr->salary = employee[nextindex].salary; 
            ptr->id = employee[nextindex].id;
            strcpy(ptr->name, employee[nextindex].name);
            strcpy(ptr->surname, employee[nextindex].surname);
        } 
        ptr++;
    }
    for(int i=0; i<n_emp-1; i++)
        ptr--;
}
//construct the function of get_largest_salary_index
int get_largest_salary_index(struct person employee[MAXNAME], int n_emp){
    int largest = 0;
    for(int i=1;i<n_emp;i++)
        if(employee[i].salary > employee[largest].salary)
            largest = i;
    return largest;
}
//construct the function of update_by_key
void update_by_key(struct person *ptr, int key, char name[], char surname[], int salary){
    for (int c = 0; c < key; c++)
        ptr++;
    strcpy(ptr->name, name);
    strcpy(ptr->surname, surname);
    ptr->salary = salary;
    for (int c = 0; c < key; c++)
        ptr--;
}
//construct the function of search_lname
int search_lname(struct person employee[MAXNAME], int n_emp, char surname[]){
    for(int i=0;i<n_emp;i++)
        if(strcasecmp(employee[i].surname, surname)==0)
            return i;
    return -1;
}
//construct the function of all_last_name
void all_last_name(struct person employee[MAXNAME], int n_emp, char surname[]){
    int flag = search_lname(employee, n_emp, surname);
    if(flag == -1){
        printf("Employee with last name %s not found in DB\n", surname);
    } else {
        printf("NAME\t\t\t\t\t\tSALARY\t\tID\n");
        printf("---------------------------------------------------------------\n");
        for(int i=0;i<n_emp;i++)
            if(strcasecmp(employee[i].surname, surname)==0)
                printf("%s\t\t%s\t\t%d\t\t%d\n",employee[i].name,employee[i].surname,employee[i].salary,employee[i].id);       
        printf("---------------------------------------------------------------\n");        
    }
}
//construct the function of print_by_name
int print_by_name(struct person employee[1024], char emp_name[256]){
    /* Given structure and last name, this function prints the employee record
    if it exists */
    int length = sizeof(employee)/ sizeof(employee[0]);
    for(int i=0;i<length;i++){
        if(employee[i].surname == emp_name){
            printf("NAME\t\t\t\tSALARY\t\tID\n");
            printf("---------------------------------------------------------------\n");
            printf("%s\t\t%s\t\t%d\t\t%d\n",employee[i].name,employee[i].surname,employee[i].salary,employee[i].id);
            printf("---------------------------------------------------------------\n");
            return 0;
        }
    }
    return 0;
}