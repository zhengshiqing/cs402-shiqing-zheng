package edu.iit.matrixint;

import java.util.Random;

public class MatrixIntTest2B {

	// Definition of row and column
	private static int row, column;
	private static int[][] m;
	// Random number tool
	private static Random r = new Random();

	public static void main(String[] args) {

		// Calculate the time to run once
		long start = System.currentTimeMillis();
		MatrixMultiply();
		long end = System.currentTimeMillis();
		System.out.println(" the time of multiplication once��" + (end - start) + "ms");

		// Calculate the time of 200000 times of operation
		int count = 200000;

		start = System.currentTimeMillis();
		for (int i = 0; i < count; i++) {
			MatrixMultiply();
		}
		end = System.currentTimeMillis();
		long usetTime = end - start;

		System.out
		.println("Multiplication operation" + count + "once time:" + usetTime + "ms," + usetTime / 1000 + "s");

	}

	private static void MatrixMultiply() {

		// the first matrix
		row = 3;
		column = 4;
		m = new int[row][column];
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				m[i][j] = r.nextInt(100);
			}
		}
		MatrixInt matrix1 = new MatrixInt(m);
		//////////////////// It's important to note that there are transposition methods here////////////////////
		matrix1.transposed();

		// the second matrix
		row = 4;
		column = 3;
		m = new int[row][column];
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				m[i][j] = r.nextInt(50);
			}
		}
		MatrixInt matrix2 = new MatrixInt(m);
		////////////////////It's important to note that there are transposition methods here////////////////////
		matrix2.transposed();

		// Judge whether it can be multiplied
		if (matrix1.column != matrix2.row) {
			System.out.println(" Error! Does not conform to the matrix algorithm! ");
		} else {
			MatrixInt matrix3 = matrix1.multiply(matrix2);
			System.out.println("matrix multiplication =");
			matrix3.print();
		}
	}

}