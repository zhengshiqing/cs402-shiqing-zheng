package edu.iit.matrixint;

//Int matrix
public class MatrixInt {

	int row, column;
	int[][] matrix;

	// Constructor
	MatrixInt(int[][] matrix) {
		this.row = matrix.length;
		this.column = matrix[0].length;
		this.matrix = new int[this.row][this.column];
		for (int i = 0; i < this.row; i++) {
			for (int j = 0; j < this.column; j++) {
				this.matrix[i][j] = matrix[i][j];
			}
		}
	}

	// Matrix multiplication
	MatrixInt multiply(MatrixInt mat2) {
		int[][] multiply_result = new int[this.row][mat2.column];
		for (int i = 0; i < this.row; i++) {
			for (int j = 0; j < mat2.column; j++) {
				for (int k = 0; k < mat2.row; k++) {
					multiply_result[i][j] += this.matrix[i][k] * mat2.matrix[k][j];
				}
			}
		}
		return new MatrixInt(multiply_result);
	}

	// Matrix transposition
	void transposed() {
		int[][] A = this.matrix;
		int[][] AT = new int[A[0].length][A.length];
		for (int i = 0; i < AT.length; i++) {
			for (int j = 0; j < AT[0].length; j++) {
				AT[i][j] = A[j][i];
			}
		}
		int tmp = row;
		this.row = this.column;
		this.column = tmp;
		this.matrix = AT;
	}

	// print function
	void print() {
		for (int i = 0; i < this.row; i++) {
			for (int j = 0; j < this.column; j++) {
				System.out.print(this.matrix[i][j] + " ");
			}
			System.out.println();
		}
	}

}
