package edu.iit.matrixdouble;

//Double Matrix
public class MatrixDouble {

	int row, column;
	double[][] matrix;

	// Constructor
	MatrixDouble(double[][] matrix) {
		this.row = matrix.length;
		this.column = matrix[0].length;
		this.matrix = new double[this.row][this.column];
		for (int i = 0; i < this.row; i++) {
			for (int j = 0; j < this.column; j++) {
				this.matrix[i][j] = matrix[i][j];
			}
		}
	}

	// Matrix multiplication
	MatrixDouble multiply(MatrixDouble mat2) {
		double[][] multiply_result = new double[this.row][mat2.column];
		for (int i = 0; i < this.row; i++) {
			for (int j = 0; j < mat2.column; j++) {
				for (int k = 0; k < mat2.row; k++) {
					multiply_result[i][j] += this.matrix[i][k] * mat2.matrix[k][j];
				}
			}
		}
		return new MatrixDouble(multiply_result);
	}

	// Matrix transportation
	void transposed() {
		double[][] A = this.matrix;
		double[][] AT = new double[A[0].length][A.length];
		for (int i = 0; i < AT.length; i++) {
			for (int j = 0; j < AT[0].length; j++) {
				AT[i][j] = A[j][i];
			}
		}
		int tmp = row;
		this.row = this.column;
		this.column = tmp;
		this.matrix = AT;
	}

	// Print function
	void print() {
		for (int i = 0; i < this.row; i++) {
			for (int j = 0; j < this.column; j++) {
				System.out.print(this.matrix[i][j] + " ");
			}
			System.out.println();
		}
	}

}
